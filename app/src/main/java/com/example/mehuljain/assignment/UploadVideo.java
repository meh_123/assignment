package com.example.mehuljain.assignment;

/**
 * Created by mehul jain on 09-05-2017.
 */

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.CursorLoader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import net.gotev.uploadservice.*;

import org.json.JSONException;
import org.json.JSONObject;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import java.io.File;

import okhttp3.MediaType;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class UploadVideo extends AppCompatActivity  {
    private static int RESULT_LOAD_VIDEO = 1;
    public TextView textViewToChange;
    String decodableString;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.upload_video);
        final Button btn_load = (Button) findViewById(R.id.buttonChoose);
        textViewToChange = (TextView) findViewById(R.id.UploadVideoResponse);
        btn_load.setOnClickListener(new View.OnClickListener()  {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                loadVideoFromGallery(btn_load);
            }
        });
    }
    /*
     * PICK THE VIDEO AND EXTRACT ITS ADDRESS
     */
    public void loadVideoFromGallery(View view)
    {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, RESULT_LOAD_VIDEO);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            // When a video is picked
            if (requestCode == RESULT_LOAD_VIDEO && resultCode == RESULT_OK
                    && null != data)
            {
                // Get the video from data
                Uri selectedVideo = data.getData();
                uploadMultipart(getApplicationContext(),selectedVideo);

            } else
            {
                Toast.makeText(this, "You haven't picked any video",
                        Toast.LENGTH_LONG).show();
            }
        } catch (Exception e)
        {
            e.printStackTrace();
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                    .show();
        }
    }

    /*
     * UPLOAD THE SELECTED VIDEO TO THE SRVER
     */

    public void uploadMultipart(final Context context,Uri selectedVideo) {
        try {
            String uploadId =
                    new MultipartUploadRequest(context, "http://52.77.240.129/mehul/upload.php")
                            .addFileToUpload(selectedVideo.toString(), "upload_video")
                            .setNotificationConfig(new UploadNotificationConfig())
                            .setMaxRetries(2)
                            .setDelegate(new UploadStatusDelegate() {
                                @Override
                                public void onProgress(Context context, UploadInfo uploadInfo) {
                                    textViewToChange.setText("progressing uploading of   videos");
                                }

                                @Override
                                public void onError(Context context, UploadInfo uploadInfo, Exception exception) {
                                    textViewToChange.setText("errror in uploading  videos");

                                }

                                @Override
                                public void onCompleted(Context context, UploadInfo uploadInfo, net.gotev.uploadservice.ServerResponse serverResponse) {
                                    Log.d("check",serverResponse.getBodyAsString());
                                    textViewToChange.setText("succesfully uploaded videos");
                                    finish();
                                    // your code here
                                    // if you have mapped your server response to a POJO, you can easily get it:
                                    // YourClass obj = new Gson().fromJson(serverResponse.getBodyAsString(), YourClass.class);

                                }

                                @Override
                                public void onCancelled(Context context, UploadInfo uploadInfo) {
                                    // your code here
                                }
                            })
                            .startUpload();
        } catch (Exception exc) {
            Log.e("AndroidUploadService", exc.getMessage(), exc);
        }
    }
}