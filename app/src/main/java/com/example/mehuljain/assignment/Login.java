package com.example.mehuljain.assignment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.mehuljain.assignment.R;
import com.squareup.otto.Subscribe;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.Callback;
import retrofit2.Response;
public class Login extends AppCompatActivity implements View.OnClickListener {
    Button loginBtn;
    EditText email,password;
    private Communicator communicator;
    SharedPreferences shared_preferences;
    SharedPreferences.Editor shared_preferences_editor;
    String str;
    private static  final String TAG="Login";
    private static final int REQUEST_HOME=1;
    private boolean RETURNED_LOGIN=false;
    //    private static final String USER_NAME = "com.chalkstreet.mehuljain.first_app.MESSAGE";
    public RelativeLayout sign_in_loading_progress_bar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        Log.d(TAG,"Inside oncreate");

        setContentView(R.layout.activity_login);
        communicator = new Communicator();

        sign_in_loading_progress_bar = (RelativeLayout) findViewById(R.id.sign_in_loading_progress_bar);
        sign_in_loading_progress_bar.setVisibility(View.INVISIBLE);
        loginBtn = (Button)findViewById(R.id.loginBtn);
        email=(EditText)findViewById(R.id.email);
        password=(EditText)findViewById(R.id.password);

        loginBtn.setOnClickListener(this);

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(resultCode!= Activity.RESULT_CANCELED){
            return;
        }
//        if(requestCode==REQUEST_HOME)
//        {
//            if(data== null)
//            {
//                return;
//            }
//            Log.d(TAG,"inside onActivityResult");
//            RETURNED_LOGIN=HomeActivity.return_login(data);
//            password.setText(null);
//        }
    }
    @Override
    protected void onStart(){
        super.onStart();
        Log.d(TAG,"Inside On start");
    }
    @Override
    protected void onPause(){
        super.onPause();
        BusProvider.getInstance().unregister(this);
        Log.d(TAG,"Inside On Pause");
    }
    @Override
    protected void onResume(){
        super.onResume();
        BusProvider.getInstance().register(this);
        Log.d(TAG,"Inside On Resume");
        Log.d(TAG,"Returned Login:"+RETURNED_LOGIN);
//        Log.e(TAG,"e called");
//        Log.w(TAG,"w called");
//        Log.i(TAG,"i called");
//        Log.v(TAG,"verbose called");

    }
    @Override
    protected void onStop(){
        super.onStop();
        Log.d(TAG,"Inside On stop");
    }
    @Override
    protected void onDestroy(){
        super.onDestroy();
        Log.d(TAG,"Inside On Destroy");
    }
    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.loginBtn:
            {
                sign_in_loading_progress_bar.setVisibility(View.VISIBLE);
                usePost(email.getText().toString(), password.getText().toString());

////                    Toast.makeText(getApplication(),"processing",Toast.LENGTH_SHORT).show();
//                if(email.getText().toString().trim().equals("mehul") && password.getText().toString().trim().equals("1")){
//                    sign_in_loading_progress_bar.setVisibility(View.INVISIBLE);
//
//                    Toast.makeText(getApplication(),"Logged in",Toast.LENGTH_SHORT).show();
//                    String str="HI,"+email.getText().toString().trim();
////                        Intent intent =new Intent(Login.this,HomeActivity.class);
//                    Intent intent=HomeActivity.newIntent(Login.this,str);
//                    //startActivity(intent);
//                    startActivityForResult(intent,REQUEST_HOME);
//                }
//                else {
//                    sign_in_loading_progress_bar.setVisibility(View.VISIBLE);
//                    Toast.makeText(getApplication(),"Wrong Credential",Toast.LENGTH_SHORT).show();
//                }
            }
            break;
        }
    }

    private void usePost(String username, String password){
        communicator.loginPost(username, password);
    }
    @Subscribe
    public void onServerEvent(ServerEvent serverEvent){
        Toast.makeText(this, ""+serverEvent.getServerResponse().getMessage(), Toast.LENGTH_SHORT).show();
        if(serverEvent.getServerResponse().getUsername() != null){
            Log.d("check",serverEvent.getServerResponse().getUid());
            shared_preferences = getApplicationContext().getSharedPreferences("User_Pref", MODE_PRIVATE);
            shared_preferences_editor  = shared_preferences.edit();
            shared_preferences_editor.putString("email",serverEvent.getServerResponse().getUsername());
            shared_preferences_editor.commit();
            Intent intent =new Intent(Login.this,SectionView.class);
            startActivity(intent);
            finish();
//            information.setText("Username: "+serverEvent.getServerResponse().getUsername() + " || Password: "+serverEvent.getServerResponse().getPassword());
        }
//        extraInformation.setText("" + serverEvent.getServerResponse().getMessage());
    }

    @Subscribe
    public void onErrorEvent(ErrorEvent errorEvent){
        Toast.makeText(this,""+errorEvent.getErrorMsg(),Toast.LENGTH_SHORT).show();
    }
}