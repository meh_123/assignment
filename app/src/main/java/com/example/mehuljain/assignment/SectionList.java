package com.example.mehuljain.assignment;

/**
 * Created by mehul jain on 09-05-2017.
 */

public class SectionList {
    int section_id,course_id,section_status,section_order;
    String Section_name;

    public int getSection_id() {
        return section_id;
    }

    public int getCourse_id() {
        return course_id;
    }

    public int getSection_order() {
        return section_order;
    }

    public String getSection_name() {
        return Section_name;
    }

    public int getSection_status() {
        return section_status;
    }

    public void setSection_id(int section_id) {
        this.section_id = section_id;
    }

    public void setCourse_id(int course_id) {
        this.course_id = course_id;
    }

    public void setSection_name(String section_name) {
        Section_name = section_name;
    }

    public void setSection_order(int section_order) {
        this.section_order = section_order;
    }

    public void setSection_status(int section_status) {
        this.section_status = section_status;
    }
}
