package com.example.mehuljain.assignment;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by mehul jain on 08-05-2017.
 */

public class ServerResponse implements Serializable{
//        @SerializedName("returned_email")
        private String email;
//        @SerializedName("returned_password")
        private String password;
//        @SerializedName("message")
        private String msg;
        private String uid;
//        @SerializedName("response_code")
        private int status;

        public ServerResponse(String email, String password, String msg, int status,String uid){
            this.email = email;
            this.password = password;
            this.msg = msg;
            this.status = status;
            this.uid=uid;
        }

        public String getUsername() {
            return email;
        }
        public String getUid() {
            return uid;
        }

    public void setUsername(String username) {
            this.email = username;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getMessage() {
            return msg;
        }

        public void setMessage(String msg) {
            this.msg = msg;
        }

        public int getResponseCode() {
            return status;
        }

        public void setResponseCode(int responseCode) {
            this.status = status;
        }
    }
