package com.example.mehuljain.assignment;

/**
 * Created by mehul jain on 08-05-2017.
 */

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.Part;
import retrofit2.http.Query;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
public interface ApiInterface {
    //This method is used for "POST"
    @FormUrlEncoded
    @POST("user/login/")
    Call<ServerResponse> post(
            @Field("method") String method,
            @Field("email") String username,
            @Field("password") String password
    );

    //This method is used for "GET"
//    @GET("/api.php")
//    Call<ServerResponse> get(
//            @Query("method") String method,
//            @Query("email") String username,
//            @Query("password") String password
//    );
}
