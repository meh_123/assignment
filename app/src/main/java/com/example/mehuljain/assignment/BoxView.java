package com.example.mehuljain.assignment;

/**
 * Created by mehul jain on 08-05-2017.
 */
import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BoxView extends AppCompatActivity{
    RecyclerView cardLayout;
    List<BoxList> BoxList;
    RecyclerView.Adapter imageHolder;
    ImageButton add_card_main;
    LinearLayoutManager card_manager;
    DisplayMetrics displayMetrics = new DisplayMetrics();
    int current_card_position;
    private int GET_IMAGE_REQUEST = 1;
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private Bitmap bitmap;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    //
//    @BindView(R.id.cardView) RecyclerView cardLayout;
//    @BindView(R.id.buttonAddMain) ImageButton add_card_main;
//    @BindView(R.id.buttonAdd) ImageButton add_card;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        ButterKnife.bind(this);
        BoxList = new ArrayList<>();
        cardLayout = (RecyclerView)findViewById(R.id.cardView);
        add_card_main = (ImageButton)findViewById(R.id.buttonAddMain);
        cardLayout.setHasFixedSize(true);
        final LinearLayoutManager card_manager = new LinearLayoutManager(getApplicationContext());
        card_manager.setOrientation(LinearLayoutManager.VERTICAL);
        cardLayout.setLayoutManager(card_manager);
        BoxList.add(BoxList.size(),new BoxList());
        imageHolder = new ImageHolder(getApplication(), BoxList);
        imageHolder.setHasStableIds(false);
        cardLayout.setAdapter(imageHolder);


        add_card_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BoxList.add(BoxList.size(),new BoxList());
                imageHolder.notifyItemInserted(BoxList.size()-1);
                cardLayout.scrollToPosition(BoxList.size()-1);
                Log.d("size of cards",BoxList.size()+"");
            }
        });

        verifyStoragePermissions(this);
    }

    public class ImageHolder extends RecyclerView.Adapter<BoxView.ImageHolder.viewHolder>{
        private Context context;
        public List<BoxList> BoxList;
        public ImageHolder(Context context , List<BoxList> list) {
            this.context = context;
            this.BoxList = list;
        }

        @Override
        public BoxView.ImageHolder.viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).
                    inflate(R.layout.box_card, parent, false);

            return new viewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final BoxView.ImageHolder.viewHolder holder, final int position) {

            if (BoxList.get(holder.getAdapterPosition()).getBox_content_id() !=0) {
                holder.BoxView.setVisibility(View.VISIBLE);
                holder.box_name.setText("BOX "+(holder.getAdapterPosition()+1)+"");
            } else {
                holder.box_name.setText("BOX "+(holder.getAdapterPosition()+1)+"");
                holder.BoxView.setVisibility(View.INVISIBLE);
                Log.d("reached", current_card_position + "");
                holder.remove_card.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //remove the Current Card from the list
                        try {
                            current_card_position = holder.getAdapterPosition();
                            Log.d("removed card at ", current_card_position + "");
                            BoxList.remove(holder.getAdapterPosition());
                            imageHolder.notifyItemRemoved(holder.getAdapterPosition());
                            imageHolder.notifyItemRangeChanged(holder.getAdapterPosition(), BoxList.size());
                            Toast.makeText(getApplication(),"Removed Section at Position "+(current_card_position+1),Toast.LENGTH_SHORT).show();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });
                holder.add_box.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.d("check","called");
                        //remove the Current Card from the list
                        try {
                            current_card_position = holder.getAdapterPosition();
                            Intent intent =new Intent(BoxView.this,UploadVideo.class);
                            startActivity(intent);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });
            }
        }

        @Override
        public int getItemCount() {
            return BoxList.size();
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        public class viewHolder extends RecyclerView.ViewHolder{
            ImageButton add_box,BoxView;
            ImageButton remove_card;
            TextView box_name;
            public viewHolder(View itemView) {
                super(itemView);
                // bind all views
                BoxView = (ImageButton)itemView.findViewById(R.id.boxView);
                add_box = (ImageButton)itemView.findViewById(R.id.AddBox);
                remove_card = (ImageButton)itemView.findViewById(R.id.removeCard);
                box_name = (TextView)itemView.findViewById(R.id.box_name);

            }
        }
    }

    @Override
    // on activity result after selecting image from gallery  set image to view
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GET_IMAGE_REQUEST && data != null && data.getData() != null)
        {
            Uri filePath = data.getData();
            // start cropping activity for pre-acquired image saved on the device
        }
    }
    public void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 1);
            }
        }
    }
    // getter
    public List<BoxList> BoxList() {
        return BoxList;
    }
    // setter
    public void BoxList(List<BoxList> BoxList) {
        this.BoxList = BoxList;
    }
}
