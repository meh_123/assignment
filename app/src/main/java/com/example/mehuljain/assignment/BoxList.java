package com.example.mehuljain.assignment;

/**
 * Created by mehul jain on 09-05-2017.
 */

public class BoxList {
    int box_id,section_id,box_status,box_order,box_content_id;
    String box_name,box_duartion;
    public int getBox_id(){
        return box_id;
    }

    public int getSection_id() {
        return section_id;
    }

    public int getBox_content_id() {
        return box_content_id;
    }

    public int getBox_status() {
        return box_status;
    }

    public String getBox_duartion() {
        return box_duartion;
    }

    public int getBox_order() {
        return box_order;
    }

    public String getBox_name() {
        return box_name;
    }

    public void setBox_content_id(int box_content_id) {
        this.box_content_id = box_content_id;
    }

    public void setBox_id(int box_id) {
        this.box_id = box_id;
    }

    public void setBox_duartion(String box_duartion) {
        this.box_duartion = box_duartion;
    }

    public void setBox_name(String box_name) {
        this.box_name = box_name;
    }

    public void setBox_order(int box_order) {
        this.box_order = box_order;
    }

    public void setBox_status(int box_status) {
        this.box_status = box_status;
    }

    public void setSection_id(int section_id) {
        this.section_id = section_id;
    }
}
