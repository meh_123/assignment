package com.example.mehuljain.assignment;

import com.squareup.otto.Bus;

/**
 * Created by mehul jain on 08-05-2017.
 */

public class BusProvider {

    private static final Bus BUS = new Bus();

    public static Bus getInstance(){
        return BUS;
    }

    public BusProvider(){}
}