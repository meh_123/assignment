package com.example.mehuljain.assignment;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;

/**
 * Created by mehul jain on 09-05-2017.
 */

public interface UploadApiService
{
    @Multipart
    @POST("upload.php")
    Call<ResponseBody> uploadVideo(
            @Part("description") String description,
            @Part(("videos\"; filename=\"a.mp4\" ")) RequestBody video);
}